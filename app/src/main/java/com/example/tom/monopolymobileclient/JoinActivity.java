package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.example.tom.monopolymobileclient.model.Invite;
import com.example.tom.monopolymobileclient.services.GameService;
import com.example.tom.monopolymobileclient.services.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class JoinActivity extends Activity  {

    private SharedPreferences sharedPreferences;
    private UserService userService;
    private GameService gameService;
    private ArrayList<Invite> invites;
    private Gson gson;
    private ListView lvInvites;
    private ArrayAdapter<Invite> inviteAdapter;

    private BroadcastReceiver onBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent i) {
            try {
                JSONArray inviteData = new JSONArray(i.getStringExtra("invites"));
                invites = gson.fromJson(inviteData.toString(), new TypeToken<List<Invite>>(){}.getType());
                inviteAdapter = new ArrayAdapter<Invite>(ctxt,android.R.layout.simple_list_item_1, invites);
                lvInvites.setAdapter(inviteAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        registerReceiver(onBroadcast, new IntentFilter("sendInvites"));
        this.lvInvites = findViewById(R.id.lvInvites);
        this.gson = new Gson();
        this.userService = new UserService(getApplicationContext());
        this.gameService = new GameService(getApplicationContext());
        this.gameService.connect();
        sharedPreferences = getApplicationContext().getSharedPreferences("userPreferences", 0);
        JWT parsedJWT = new JWT(sharedPreferences.getString("accessToken", null));
        Claim subscriptionMetaData = parsedJWT.getClaim("username");
        String username = subscriptionMetaData.asString();
        userService.getInvites(username);

        lvInvites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Invite invite = invites.get(position);
                gameService.addPlayer(invite.getGameId(), invite.getUserId());
                Intent intent = new Intent(JoinActivity.this, LobbyActivity.class);
                startActivity(intent);
            }
        });
    }


}
