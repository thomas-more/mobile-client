package com.example.tom.monopolymobileclient.model;

public class Player {
    private int id;
    private String name;
    private String pawn;
    int position;
    int money;
    private boolean isInJail;

    public Player(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        if(pawn == null){
            return name;
        }

        return name + " " + pawn;
    }

    public int getId() {
        return id;
    }

    public String getPawn() {
        return pawn;
    }

    public void setPawn(String pawn) {
        this.pawn = pawn;
    }

    public String getName() {
        return name;
    }
}
