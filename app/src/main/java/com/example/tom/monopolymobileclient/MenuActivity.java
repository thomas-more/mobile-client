package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.tom.monopolymobileclient.model.Game;
import com.example.tom.monopolymobileclient.services.GameService;
import com.example.tom.monopolymobileclient.services.UserService;

import java.io.IOException;

public class MenuActivity extends Activity {

    private Button btnProfile;
    private Button btnStart;
    private Button btnJoin;
    private GameService gameService;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        gameService = new GameService(getApplicationContext());
        btnProfile = findViewById(R.id.btnProfile);
        btnStart = findViewById(R.id.btnStart);
        btnJoin = findViewById(R.id.btnJoin);
        sharedPreferences = getApplicationContext().getSharedPreferences("userPreferences", 0);
        editor = sharedPreferences.edit();
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.remove("gameId").apply();
                Intent intent = new Intent(MenuActivity.this, LobbyActivity.class);
                startActivity(intent);
                gameService.createLobby();
            }
        });
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, JoinActivity.class);
                startActivity(intent);
            }
        });
    }


}
