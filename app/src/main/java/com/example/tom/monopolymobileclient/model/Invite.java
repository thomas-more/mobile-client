package com.example.tom.monopolymobileclient.model;

public class Invite {
    private int gameId;
    private int userId;
    private String hostPlayer;

    public int getGameId() {
        return gameId;
    }

    public int getUserId() {
        return userId;
    }

    public String getHostPlayer() {
        return hostPlayer;
    }

    @Override
    public String toString() {
        return "gameId: " + gameId;
    }
}
