package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tom.monopolymobileclient.services.GameService;

public class EmailInviteActivity extends Activity {

    private Button btnEmailInvite;
    private EditText editEmail;
    private GameService gameService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_invite);
        gameService = new GameService(getApplicationContext());
        btnEmailInvite = findViewById(R.id.btnEmailInvite);
        editEmail = findViewById(R.id.editEmail);

        btnEmailInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailInviteActivity.this, LobbyActivity.class);
                startActivity(intent);
                String email = editEmail.getText().toString();
                gameService.inviteEmailPlayer(email);
            }
        });
    }
}
