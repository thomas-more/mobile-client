package com.example.tom.monopolymobileclient.security;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.tom.monopolymobileclient.model.User;
import com.example.tom.monopolymobileclient.services.UserService;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    private final SharedPreferences sharedPreferences;

    public HeaderInterceptor(Context context) {
        this.sharedPreferences = context.getApplicationContext().getSharedPreferences("userPreferences",0);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        if (sharedPreferences.getString("accessToken", null) != null && !sharedPreferences.getString("accessToken", null).isEmpty()){
            request = request.newBuilder()
                    .addHeader("Authorization", "Bearer " + sharedPreferences.getString("accessToken", null))
                    .build();
            Log.w("Authorization", "Intercepted:   " + sharedPreferences.getString("accessToken",null));
        }
        return chain.proceed(request);
    }
}
