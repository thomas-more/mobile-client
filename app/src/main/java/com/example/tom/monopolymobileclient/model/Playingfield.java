package com.example.tom.monopolymobileclient.model;

public class Playingfield {
    private int id;
    private String name;
    private int buyValue;
    private int rentUnbuild;
    private int rentOneHouses;
    private int rentTwoHouses;
    private int rentThreeHouses;
    private int rentFourHouses;
    private int rentHotel;

   /* private String street;


    private int mortgage;
    private int priceHouseHotel;
    private int ownedBy;
    private int housesAmount;
    private int priceOneUtility;
    private int priceTwoUtilities;
    private int priceOneStation;
    private int priceTwoStations;
    private int priceThreeStations;
    private int priceFourStations;
    private Boolean inMortgage;*/

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBuyValue() {
        return buyValue;
    }

    public int getRentUnbuild() {
        return rentUnbuild;
    }

    public int getRentOneHouses() {
        return rentOneHouses;
    }

    public int getRentTwoHouses() {
        return rentTwoHouses;
    }

    public int getRentThreeHouses() {
        return rentThreeHouses;
    }

    public int getRentFourHouses() {
        return rentFourHouses;
    }

    public int getRentHotel() {
        return rentHotel;
    }
}
