package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tom.monopolymobileclient.model.User;
import com.example.tom.monopolymobileclient.services.UserService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class  RegisterActivity extends Activity {

    private UserService userService;
    private EditText editUsername;
    private EditText editEmail;
    private EditText editPassword;
    private EditText editConfirmPassword;
    private Button registerButton;
    private TextView tvOpenLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        userService = new UserService(getApplicationContext());
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_register);

        editUsername = (EditText) findViewById(R.id.editUsername);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);
        registerButton = (Button) findViewById(R.id.registerButton);
        tvOpenLogin = (TextView) findViewById(R.id.tvOpenLogin);

        editUsername.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    if (editUsername.getText().toString().length() ==0){
                        editUsername.setError("Username required!");
                    }
                    checkValid();
                }
            }

        });

        editEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                 if (!hasFocus){
                     if (!isValidEmail(editEmail.getText().toString())){
                        editEmail.setError("Not a valid email");
                     }
                     checkValid();
                 }
            }
        });

        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    if (editPassword.getText().toString().length() < 8){
                        editPassword.setError("Password needs to be at least 8 characters");
                    }
                    checkValid();
                }
            }
        });

        editConfirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    if (!editPassword.getText().toString().equals(editConfirmPassword.getText().toString())){
                        editConfirmPassword.setError("Passwords do not match");
                    }
                    checkValid();
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User u = new User(editUsername.getText().toString(), editPassword.getText().toString(), editEmail.getText().toString());
                userService.registerUser(u);
            }
        });

        tvOpenLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+\\.[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void checkValid(){
        if (editUsername.getError() == null && editEmail.getError() == null && editPassword.getError() == null && editConfirmPassword.getError() == null){
            registerButton.setEnabled(true);
        }else{
            registerButton.setEnabled(false);
        }
    }

}
