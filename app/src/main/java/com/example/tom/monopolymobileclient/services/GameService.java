package com.example.tom.monopolymobileclient.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.auth0.android.jwt.JWT;
import com.example.tom.monopolymobileclient.model.Player;
import com.example.tom.monopolymobileclient.security.HeaderInterceptor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class GameService {
    private OkHttpClient client;
    private Gson gson;
    private MediaType mediaType;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private StompClient stompClient;


    public GameService(Context context) {
        this.context = context;
        mediaType = MediaType.parse("application/json");
        this.client = new OkHttpClient.Builder().addInterceptor(new HeaderInterceptor(context)).build();
        gson = new Gson();
        sharedPreferences = context.getApplicationContext().getSharedPreferences("userPreferences", 0);
        editor = sharedPreferences.edit();
    }

    public void connect() {
        stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "ws://10.0.2.2:8080/socket/websocket?jwt=" + sharedPreferences.getString("accessToken", null));
        stompClient.connect();
        stompClient.topic("/lobby" + sharedPreferences.getInt("gameId", 0)).subscribe(res -> {
            JSONObject jsonObject = new JSONObject(res.getPayload());
            Intent i = new Intent("sendGame");
            i.putExtra("game", jsonObject.toString());
            context.sendBroadcast(i);

        }, throwable -> {
            Log.e("Error", "Error on subscribe topic", throwable);
        });
    }

    public void createLobby() {
        System.out.println("lobby/create/true");
        String token = sharedPreferences.getString("accessToken", null);
        JWT jwt = new JWT(token);
        String username =jwt.getClaim("username").asString();
        System.out.println("lobby/create/username " + username);
        RequestBody body = RequestBody.create(mediaType, (username))
                ;
        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/api/lobby/create")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("failureerror: " + e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                JSONObject jsonObject = null;
                String data = response.body().string();
                try {
                    jsonObject = new JSONObject(data);
                    System.out.println("jsonObject: " + jsonObject);
                    editor.putInt("gameId", Integer.parseInt(jsonObject.getString("id")));
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent("sendGame");
                i.putExtra("game", data);
                context.sendBroadcast(i);
            }
        });

    }


    public void inviteEmailPlayer(String email) {
        JSONObject o = new JSONObject();
        try {
            o.put("gameId", sharedPreferences.getInt("gameId", 0));
            o.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(mediaType, o.toString());
        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/api/lobby/invite")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    public void getGame(int gameId) {
        System.out.println("jabinneu");
        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/game/" + gameId)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                Intent i = new Intent("sendGame");
                i.putExtra("game", response.body().string());
                context.sendBroadcast(i);
            }
        });
    }

    public void addPlayer(int gameId, int userId){
        JSONObject jObjectData = new JSONObject();
        try {
            jObjectData.put("gameId", gameId);
            jObjectData.put("userId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        stompClient.send("/api/lobby/add",jObjectData.toString()).subscribe();
    }

    public void pickPawn(String pawn, int id){
        JSONObject jObjectDate = new JSONObject();

        try{
            jObjectDate.put("gameId",sharedPreferences.getInt("gameId", 0));
            jObjectDate.put("pawn", pawn);
            jObjectDate.put("playerId", id);
        }catch (JSONException e) {
            e.printStackTrace();
        }

        stompClient.send("/api/pickPawn", jObjectDate.toString()).subscribe();
    }
}

