package com.example.tom.monopolymobileclient.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.auth0.android.jwt.JWT;
import com.example.tom.monopolymobileclient.LoginActivity;
import com.example.tom.monopolymobileclient.MenuActivity;
import com.example.tom.monopolymobileclient.model.Invite;
import com.example.tom.monopolymobileclient.model.User;
import com.example.tom.monopolymobileclient.security.HeaderInterceptor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserService {

    private OkHttpClient client;
    private MediaType mediaType;
    private Gson gson;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public UserService(Context context) {
        this.client = new OkHttpClient.Builder().addInterceptor(new HeaderInterceptor(context)).build();
        mediaType = MediaType.parse("application/json");
        gson = new Gson();
        sharedPreferences = context.getApplicationContext().getSharedPreferences("userPreferences", 0);
        editor = sharedPreferences.edit();
        this.context = context;
    }

    public void registerUser(final User user) {
        RequestBody body = RequestBody.create(mediaType, gson.toJson(user));
        Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/api/users/register")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("test", "error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.i("test", response.message());
            }
        });
    }

    public void loginUser(final User user) {
        RequestBody body = RequestBody.create(mediaType, gson.toJson(user));
        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/api/users/login")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i("test", "error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    editor.putString("accessToken", jsonObject.getString("accessToken"));
                    editor.commit();
                    Intent intent = new Intent(context, MenuActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean isLoggedIn() {
        String token = sharedPreferences.getString("accessToken", null);
        if (token == null || token.isEmpty()){
            return false;
        }
        JWT jwt = new JWT(token);
        Date expiresAt = jwt.getExpiresAt();
        return expiresAt.after(Calendar.getInstance().getTime());
    }

    public void getInvites(String username) {

        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8080/api/users/invites/" + username)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Intent i = new Intent("sendInvites");
                i.putExtra("invites", response.body().string());
                context.sendBroadcast(i);
            }
        });
    }

    public void logout(){
        editor.remove("accessToken");
        editor.commit();
    }
}




