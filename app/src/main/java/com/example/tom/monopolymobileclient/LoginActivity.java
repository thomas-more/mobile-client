package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tom.monopolymobileclient.model.User;
import com.example.tom.monopolymobileclient.services.UserService;

public class LoginActivity extends Activity {

    private TextView tvRegister;
    private Button btnLogin;
    private UserService userService;
    private EditText editUsername;
    private EditText editPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        tvRegister = (TextView) findViewById(R.id.tvRegisterActivity);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        userService = new UserService(getApplicationContext());
        editUsername = findViewById(R.id.editUsernameLogin);
        editPassword = findViewById(R.id.editPasswordLogin);


        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User u = new User(editUsername.getText().toString(),editPassword.getText().toString());
                userService.logout();
                userService.loginUser(u);
                if (userService.isLoggedIn()){

                }
            }
        });

    }


}
