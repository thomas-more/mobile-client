package com.example.tom.monopolymobileclient.api;

import com.example.tom.monopolymobileclient.model.Playingfield;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface FieldClient {
    @GET("/fields/get/5")
   Call<List<Playingfield>> getPlayingFields(@Header("Authorization")String token);}
