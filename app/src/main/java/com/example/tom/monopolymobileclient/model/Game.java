package com.example.tom.monopolymobileclient.model;

import java.util.List;

public class Game {
    private int id;
    private List<Player> players;
    private List<Playingfield> fields;
    private Player playerPlaying;
    private int maxTurnTime;
    private int parkingPot;

    public List<Player> getPlayers() {
        return players;
    }
}
