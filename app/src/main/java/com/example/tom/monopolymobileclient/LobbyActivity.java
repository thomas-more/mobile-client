package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.auth0.android.jwt.JWT;
import com.example.tom.monopolymobileclient.model.Game;
import com.example.tom.monopolymobileclient.model.Invite;
import com.example.tom.monopolymobileclient.model.Player;
import com.example.tom.monopolymobileclient.services.GameService;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class LobbyActivity extends Activity {

    private GameService gameService;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Button btnAddPlayer;
    private Button btnReturnMenu;
    private Button btnStartGame;
    private Gson gson;
    private Game game;
    private ListView lvPlayers;
    private ArrayAdapter<Player> playersAdapter;
    private BroadcastReceiver onBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent i) {
            try {
                JSONObject data = new JSONObject(i.getStringExtra("game"));
                game = gson.fromJson(data.toString(),Game.class);
                String token = sharedPreferences.getString("accessToken", null);
                JWT jwt = new JWT(token);
                String username =jwt.getClaim("username").asString();
                System.out.println(username);


                for (Player player : game.getPlayers()) {
                    if(player.getName().equals(username)){
                        player.setPawn("wheelbarrow");
                        gameService.pickPawn("wheelbarrow", player.getId());
                    }


                }

                playersAdapter = new ArrayAdapter<Player>(ctxt,android.R.layout.simple_list_item_1, game.getPlayers());
                lvPlayers.setAdapter(playersAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        registerReceiver(onBroadcast, new IntentFilter("sendGame"));
        gson = new Gson();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        this.lvPlayers = findViewById(R.id.lvPlayers);
        btnAddPlayer = findViewById(R.id.btnAddPlayer);
        btnReturnMenu = findViewById(R.id.btnReturnMenu);
        btnStartGame = findViewById(R.id.btnStartGame);
        gameService = new GameService(getApplicationContext());
        sharedPreferences = getApplicationContext().getSharedPreferences("userPreferences", 0);
        editor = sharedPreferences.edit();
        gameService.connect();
        if (sharedPreferences.getInt("gameId",0) != 0){
            gameService.getGame(sharedPreferences.getInt("gameId",0));
        }


        btnAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LobbyActivity.this, InviteActivity.class);
                startActivity(intent);
            }
        });

        btnReturnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LobbyActivity.this, MenuActivity.class);
                startActivity(intent);
            }
        });

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(onBroadcast);
    }
}
