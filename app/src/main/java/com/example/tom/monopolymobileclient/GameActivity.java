package com.example.tom.monopolymobileclient;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.example.tom.monopolymobileclient.api.FieldClient;
import com.example.tom.monopolymobileclient.model.Playingfield;
import com.example.tom.monopolymobileclient.services.GameService;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.List;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class GameActivity extends Activity {

    private OkHttpClient client;
    private Button btnEndTurn;
    private Button btnBuy;
    private ImageView imgDices;
    private final String baseURL = "http://10.0.2.2:8080/";
    private Context context;
    //private final SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("userPreferences",0);
    private static List<Playingfield> fields;
    private TextView[] textViews;
    private Integer[] specialsFields = {1,3,5,6,8,11,13,16,18,21,23,26,29,31,34,36,37,39};
    private List<Integer> specialsFieldIds = Arrays.asList(specialsFields);
    private TextView tvCardheader;
    private TextView tvBuyvalue;
    private TextView tvRentUnbuild;
    private TextView tvRentOnehouse;
    private TextView tvRentTwoHouses;
    private TextView tvRentThreeHouses;
    private TextView tvRentFourHouses;
    private TextView tvRentHotel;
    private GameService gameService;
    private StompClient stompClient;
    private SharedPreferences sharedPreferences;
    private Integer gameid;
    private Integer playerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initialiseNodes();
        gameService = new GameService(getApplicationContext());
        gameService.connect();
        sharedPreferences = getApplicationContext().getSharedPreferences("userPreferences",0);
        fieldCall();
        addEventhandlers();
    }


   private void fieldCall() {
        String token = sharedPreferences.getString("accessToken", null);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        FieldClient client = retrofit.create(FieldClient.class);
        Call<List<Playingfield>> call = client.getPlayingFields("Bearer " + token);
        call.enqueue(new Callback<List<Playingfield>>() {
            @Override
            public void onResponse(Call<List<Playingfield>> call, Response<List<Playingfield>> response) {
                fields = response.body();
                Log.d("succes", "gelukt");
                setFieldText();
            }

            @Override
            public void onFailure(Call<List<Playingfield>> call, Throwable t) {
                Log.d("failed", "failed");
            }
        });
    }

    private void addEventhandlers() {
        fillTextviewArray();
        for (int i = 0;i<textViews.length;i++) {
            final int finalI = i;
            textViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonShowPopupWindowClick(v, finalI);
                }
            });
        }

        btnEndTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTurn();
            }
        });

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyProperty();
            }
        });

        imgDices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                roll_dice();
            }
        });

    }

    private void buyProperty() {
            stompClient.send("/api/buyProperty", gameid.toString());
    }

    private void endTurn() {
        stompClient.send("/api/endTurn", gameid.toString());
    }

    private void fillTextviewArray() {
        for (int i = 0;i<textViews.length;i++) {
            String textViewID = "field" + (i+1);
            int resID = getResources().getIdentifier(textViewID, "id", getPackageName());
            textViews[i] = ((TextView) findViewById(resID));
        }
    }

    private void setFieldText() {

        for (int i = 0;i< textViews.length;i++) {
            fillTextviewArray();
            String naam = fields.get(i).getName();
            int id = fields.get(i).getId();
            if (!specialsFieldIds.contains(id)) {
            textViews[i].setText(naam);
        }}
    }

    private void roll_dice() {
        JSONObject jsonObjectData = new JSONObject();
        try {
            jsonObjectData.put("gameId", gameid);
            jsonObjectData.put("playerId", playerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        stompClient.send("/api/roll", jsonObjectData.toString()).subscribe();
    }

    private void initialiseNodes() {
        textViews = new TextView[40];
        btnEndTurn = findViewById(R.id.btnEndTurn);
        btnBuy = findViewById(R.id.btnBouwen);
        imgDices = findViewById(R.id.imgDices);
        //gameid = sharedPreferences.getInt("gameId", 0);
    }

//sharedPreferences.getString("accessToken", null)
    public void onButtonShowPopupWindowClick(View view, Integer id) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.card_detail, null);
        tvCardheader = (TextView) popupView.findViewById(R.id.cardHeader);
        tvBuyvalue = (TextView) popupView.findViewById(R.id.tvBuyvalue);
        tvRentUnbuild = (TextView) popupView.findViewById(R.id.tvRentUnbuild);
        tvRentOnehouse = (TextView) popupView.findViewById(R.id.tvRentOneHouse);
        tvRentTwoHouses = (TextView) popupView.findViewById(R.id.tvRentTwoHouses);
        tvRentThreeHouses = (TextView) popupView.findViewById(R.id.tvRentThreeHouses);
        tvRentFourHouses = (TextView) popupView.findViewById(R.id.tvRentFourHouses);
        tvRentHotel = (TextView) popupView.findViewById(R.id.tvRentHotel);



        String name = fields.get(id).getName();
        String buyvalue = String.valueOf(fields.get(id).getBuyValue());
        String rentUnbuild  = String.valueOf(fields.get(id).getRentUnbuild());
        String rentOneHouses = String.valueOf(fields.get(id).getRentOneHouses());
        String rentTwoHouses = String.valueOf(fields.get(id).getRentTwoHouses());
        String rentThreeHouses = String.valueOf(fields.get(id).getRentThreeHouses());
        String rentFourHouses = String.valueOf(fields.get(id).getRentFourHouses());
        String rentHotel = String.valueOf(fields.get(id).getRentHotel());

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup windoww
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        tvCardheader.setText(name);
        if (! buyvalue.equals("0")) {
            tvBuyvalue.setText("Buyvalue: " + buyvalue);
            tvRentUnbuild.setText("Rent unbuild: " + rentUnbuild);
            tvRentOnehouse.setText("Rent one house: " + rentOneHouses);
            tvRentTwoHouses.setText("Rent two houses: " + rentTwoHouses);
            tvRentThreeHouses.setText("Rent three houses: " + rentThreeHouses);
            tvRentFourHouses.setText("Rent one house: " + rentFourHouses);
            tvRentHotel.setText("Rent hotel: " + rentHotel);
        }

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }
}
